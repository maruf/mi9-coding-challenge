# README #

### What is this repository for? ###

A very simple nodejs server accepting only POST request in root path with [json payload](https://github.com/mi9/coding-challenge-samples/blob/master/sample_request.json) and returns a filtered [response](https://github.com/mi9/coding-challenge-samples/blob/master/sample_response.json)

### How do I get set up? ###

* Works with nodejs v0.10.28
* Has dependency on express(v3.4.4) module and included as part of source so that it can be readily deployed.
* To run just clone the repo and type in 
```
$ node server.js
```
* To run some simple tests to check server request/response

```
$ node server-test.js
```

It should not throw any assert failed error.
Note: the running server will show a log showing parsing error, but thats expected as one test is testing invalid json.

### Deploy to [OpenShift](https://www.openshift.com/) ###

* Clone this repo
* Create an app with your openshift account with [nodejs 0.10 cartridge](https://www.openshift.com/developers/node-js) 
* Add newly created git url as remote to this repo with

```
$ git remote add openshift ssh://yourapp_host_id@your-app.rhcloud.com/~/git/yourapp.git/
```
* Git pull and resolve conflict by keeping whole file from your local repo

```
$ git pull openshift/master
```

* deploy by pushing to openshift remote

```
$ git push openshift master
```

* Current revision is deployed at http://mi9challenge-marufrahman.rhcloud.com

### Who do I talk to? ###

* Maruf Rahman