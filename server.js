var http = require('http');
var express = require('express');
/**
	*  this is our solution : accepts only json and return filtered json and error reporting for invalid json
	*	assuming we are using express
*/
function solutionRequestHandler(req, res) {
	//check mime-type
	if(!req.is('json')) {
		res.json(400, {error: 'Content type is not json: check Content-Type header' });
		return;
	}
	else if(!req.accepts('json')) {
		res.json(406, {error: 'Client does not accept json: check accept header' } );
		return;
	}
					
	req.body='';
	req.on('data', function(chunk) {  req.body+=chunk;  });
	req.on('end', function()  { 
		//try parse payload and filter records
		try {
			var jsonData=JSON.parse(req.body);
			var resultRecords=jsonData.payload.filter(function(r)  {  
				return r.drm==true && r.episodeCount>0; //explicitly checking true/not truthy values for drm
			}).map(function (r) {
				return {image: r.image.showImage, slug:r.slug, title:r.title };
			});
		}
		catch(e) { //parsing failed or json does not have proper format
			res.json(400, {error: 'Could not decode request: JSON parsing failed' });
			
			//don't just eat up the exception, we may need to look for other server errors i.e. server memory issues ..
			throw e;
		}
		
		//if came here then send the json response
		res.json({response: resultRecords});
	});	
}





//plumbing code to start the app up and running with express
var ChallengeApp=function() {
	//  Scope.
    var self = this;			
	self.initialize=function()   {
		//  Set the environment variables we need.
        self.ipaddress = process.env.OPENSHIFT_NODEJS_IP;
        self.port      = process.env.OPENSHIFT_NODEJS_PORT || 8080;

        if (typeof self.ipaddress === "undefined") {            
            //  allows us to run/test the app locally.
            self.ipaddress = "127.0.0.1";
        };	
		self.app = express();
		
		//only accept routing at root allow only POST and use our solution middleware
        self.app.post('/', solutionRequestHandler);
	};
	
	/**
     *  Start the server (Launches the application).
     */
    self.start = function() {    
        self.app.listen(self.port, self.ipaddress, function() {
            console.log('%s: Node server started on %s:%d ...', Date(Date.now() ), self.ipaddress, self.port);
        });
    };
};

//run the server
var app = new ChallengeApp();
app.initialize();
app.start();

//prevent process exit on uncaught exception
process.on('uncaughtException', function (err) {
	//todo: log error
	console.log('Caught exception: ' + err);
});